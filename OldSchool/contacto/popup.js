var btnAbrirPopup = document.getElementById('btn-abrir-popup'),
	overlay = document.getElementById('overlay'),
	popup = document.getElementById('popup'),
	btnCerrarPopup = document.getElementById('btn-cerrar-popup');

btnAbrirPopup.addEventListener('click', function(){
	overlay.classList.add('active');
	popup.classList.add('active');
});

btnCerrarPopup.addEventListener('click', function(e){
	e.preventDefault();
	overlay.classList.remove('active');
	popup.classList.remove('active');
});

function validar()
{
	var nombre,telefono,correo;
	nombre=document.getElementById("nombre").value;
	telefono=document.getElementById("movil").value;
	correo=document.getElementById("correo").value;

	expresiones= /@+.+[a-z]/;  ///expresiones comunes para validar el correo 

	if(nombre ==""|| telefono=="" || correo== "")
	{
		alert("Estos campos son obligatorios");
		return false;
	}
	else if (telefono.length >10 || telefono.length <10)
	{
		alert("Telefono no valido");
		return false; 
	}
	else if (isNaN(telefono))
	{
		alert("Este campo es con numeros ");
		return false;
	}
	else if(!expresiones.test(correo))
	{
		alert("El correo no es valido");
		return false;
	}

}