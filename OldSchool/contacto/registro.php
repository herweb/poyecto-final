<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
	<head>
		<meta http-equiv="content-type" content="text/html;charset=utf-8" />
		<title>Registro Completado</title>
		<style type="text/css">
			body {margin: 20px; 
			background-color: lightcyan;
			font-family: Verdana, Helvetica, sans-serif;
			font-size: 90%;}
			h1 {color: teal;
			border-bottom: 1px solid  rgb(76, 185, 199);}
			h2 {font-size: 1.2em;
			color: blue;}
		</style>
	</head>
	<body>
		<h1><em><?php echo $_POST['nombre']; ?></em> MUCHAS GRACIAS POR REGISTRARSE </h1>

		<p> Te has registrado con los siguientes datos:</p>

		
		<ul>
			<li><strong>Nombre:</strong> <em><?php echo $_POST['nombre']; ?></em></li>
			<li><strong>E-mail:</strong> <em><?php echo $_POST['correo']; ?></em></li>
			<li><strong>Télefono:</strong> <em><?php echo $_POST['movil']; ?></em></li>
		</ul>
		

		
	</body>
</html>